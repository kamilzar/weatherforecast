package pl.weather.server.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.weather.server.model.LocationForecast;
import pl.weather.server.service.WeatherForecastService;

import java.util.List;

@RestController
@RequestMapping("/forecast")
@RequiredArgsConstructor
@Slf4j
public class WeatherForecastController {

    private final WeatherForecastService service;

    @GetMapping("/fiveDay/postalCode/{postalCode}")
    public ResponseEntity<List<LocationForecast>> getFiveDayForecastByPostalCode(@PathVariable final String postalCode) {
        return ResponseEntity.ok(service.getFiveDayForecast(postalCode));
    }
}
