package pl.weather.server.service;

import lombok.experimental.UtilityClass;
import pl.weather.client.model.DailyForecastResponse;
import pl.weather.client.model.Temperature;
import pl.weather.server.model.DailyForecast;

import java.util.List;
import java.util.stream.Collectors;

import static org.apache.commons.collections4.ListUtils.emptyIfNull;

@UtilityClass
public class DailyForecastMapper {

    public static List<DailyForecast> mapDailyForecasts(final List<DailyForecastResponse> dailyForecastResponses) {
        return emptyIfNull(dailyForecastResponses).stream()
                .map(DailyForecastMapper::mapDailyForecast).collect(Collectors.toList());
    }

    private static DailyForecast mapDailyForecast(final DailyForecastResponse dailyForecastResponse) {
        if (dailyForecastResponse == null || dailyForecastResponse.getTemperatureRange() == null) {
            return null;
        }

        final var dailyForecast = new DailyForecast();
        final Temperature maxTemperature = dailyForecastResponse.getTemperatureRange().getMaxTemperature(); //NOSONAR
        final Temperature minTemperature = dailyForecastResponse.getTemperatureRange().getMinTemperature(); //NOSONAR

        dailyForecast.setDate(dailyForecastResponse.getDate());

        if (maxTemperature != null) {
            dailyForecast.setMaximumTemperature(maxTemperature.getValue());
            dailyForecast.setUnit(maxTemperature.getUnit());
        }
        if (minTemperature != null) {
            dailyForecast.setMinimumTemperature(minTemperature.getValue());
        }

        return dailyForecast;
    }
}
