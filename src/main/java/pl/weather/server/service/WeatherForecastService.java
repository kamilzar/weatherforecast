package pl.weather.server.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import pl.weather.client.AccuweatherClient;
import pl.weather.client.config.AccuweatherProperties;
import pl.weather.client.model.PostalCodeResponse;
import pl.weather.server.model.LocationForecast;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static java.util.Collections.EMPTY_LIST;
import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static pl.weather.WeatherForecastConfiguration.CACHE_NAME;
import static pl.weather.server.service.DailyForecastMapper.mapDailyForecasts;

@Service
@RequiredArgsConstructor
@Slf4j
public class WeatherForecastService {

    private final AccuweatherClient client;
    private final AccuweatherProperties properties;
    private final CacheManager cacheManager;
    private final CircuitBreakerFactory circuitBreakerFactory; //NOSONAR

    @Cacheable(CACHE_NAME)
    public List<LocationForecast> getFiveDayForecast(final String postalCode) {
        if (StringUtils.isEmpty(postalCode)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Postal code must not me empty");
        }

        final var circuitBreaker = circuitBreakerFactory.create("circuitbreaker");
        final List<PostalCodeResponse> postalCodes =
                circuitBreaker.run(
                        () -> client.getPostalCodes(Map.of("q", postalCode)),
                        getFiveDayForecastFallback(postalCode));

        if (isEmpty(postalCodes)) {
            log.info(format("No location found for postal code: %s", postalCode));
            return emptyList();
        }

        return postalCodes.stream().map(this::getFiveDayForecast).collect(Collectors.toList());
    }

    @SuppressWarnings("unchecked")
    private Function<Throwable, List<PostalCodeResponse>> getFiveDayForecastFallback(final String postalCode) {
        return throwable -> {
            log.error("Error making request to accuweather", throwable);
            return ofNullable(cacheManager.getCache(CACHE_NAME))
                    .map(cache -> (List<PostalCodeResponse>) cache.get(postalCode))
                    .orElse(EMPTY_LIST);
        };
    }

    private LocationForecast getFiveDayForecast(final PostalCodeResponse postalCodeResponse) {
        if (postalCodeResponse == null) {
            return null;
        }

        log.info(format("Getting five day forecast for %s", postalCodeResponse.getName()));

        final var fiveDayForecastResponse =
                client.getFiveDayForecast(postalCodeResponse.getKey(), properties.getLocale(), properties.getMetric());

        if (fiveDayForecastResponse == null || isEmpty(fiveDayForecastResponse.getDailyForecasts())) {
            log.warn(format("No forecast found for %s", postalCodeResponse.getName()));
            return null;
        }

        return LocationForecast.builder()
                .name(postalCodeResponse.getName())
                .dailyForecasts(mapDailyForecasts(fiveDayForecastResponse.getDailyForecasts())).build();
    }
}
