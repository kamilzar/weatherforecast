package pl.weather.server.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.OffsetDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DailyForecast implements Serializable {

    private Double maximumTemperature;
    private Double minimumTemperature;
    private OffsetDateTime date;
    private String unit;
}
