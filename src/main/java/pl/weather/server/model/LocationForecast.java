package pl.weather.server.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LocationForecast implements Serializable {

    private String name;
    private List<DailyForecast> dailyForecasts = new ArrayList<>();
}
