package pl.weather.client.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "pl.weather.client.accuweather")
@Data
public class AccuweatherProperties {

    private String locale;
    private Boolean metric;
}
