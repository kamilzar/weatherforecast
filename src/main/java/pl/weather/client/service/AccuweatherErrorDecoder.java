package pl.weather.client.service;

import feign.Response;
import feign.codec.ErrorDecoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

@Component
@Slf4j
public class AccuweatherErrorDecoder implements ErrorDecoder {

    @Override
    public Exception decode(final String methodKey, final Response response) {
        log.error(getErrorMessage(methodKey, response));

        return new ResponseStatusException(HttpStatus.resolve(response.status()), response.reason()); //NOSONAR
    }

    private String getErrorMessage(final String methodKey, final Response response) {
        return String.format("Status code %d, method = %s", response.status(), methodKey);
    }
}
