package pl.weather.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import pl.weather.client.model.FiveDayForecastResponse;
import pl.weather.client.model.PostalCodeResponse;

import java.util.List;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@FeignClient(name = "AccuweatherClient", url = "${pl.weather.client.accuweather.address}")
public interface AccuweatherClient {

    @GetMapping(value = "/locations/v1/postalcodes/search", produces = APPLICATION_JSON_VALUE)
    List<PostalCodeResponse> getPostalCodes(@SpringQueryMap Map<String, String> parameters);

    @GetMapping(value = "/forecasts/v1/daily/5day/{locationKey}", produces = APPLICATION_JSON_VALUE)
    FiveDayForecastResponse getFiveDayForecast(
            @PathVariable String locationKey, @RequestParam String language, @RequestParam Boolean metric);
}
