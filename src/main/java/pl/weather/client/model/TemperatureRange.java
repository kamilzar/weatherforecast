package pl.weather.client.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TemperatureRange {

    @JsonProperty("Maximum")
    private Temperature maxTemperature;

    @JsonProperty("Minimum")
    private Temperature minTemperature;
}
