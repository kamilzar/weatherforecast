package pl.weather.client.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FiveDayForecastResponse {

    @JsonProperty("DailyForecasts")
    private List<DailyForecastResponse> dailyForecasts;
}
