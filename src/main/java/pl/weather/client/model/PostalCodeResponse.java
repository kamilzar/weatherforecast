package pl.weather.client.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PostalCodeResponse {

    @JsonProperty("Key")
    private String key;

    @JsonProperty("LocalizedName")
    private String name;
}
