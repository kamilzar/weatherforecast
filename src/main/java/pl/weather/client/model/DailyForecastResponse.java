package pl.weather.client.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DailyForecastResponse {

    @JsonProperty("Temperature")
    private TemperatureRange temperatureRange;

    @JsonProperty(value = "Date")
    private OffsetDateTime date;
}
