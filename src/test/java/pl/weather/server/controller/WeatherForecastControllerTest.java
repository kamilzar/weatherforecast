package pl.weather.server.controller;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import pl.weather.server.model.DailyForecast;
import pl.weather.server.model.LocationForecast;
import pl.weather.server.service.WeatherForecastService;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@WebMvcTest(WeatherForecastController.class)
@Slf4j
class WeatherForecastControllerTest {

    private static final String LOCATION_NAME = "Bangladesz";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WeatherForecastService service;

    @DisplayName("Should get five day forecast for a location with specific postal code")
    @ParameterizedTest
    @MethodSource
    void shouldGetFiveDayForecastByPostalCode(
            final String postalCode, final List<LocationForecast> result) throws Exception {
        log.info("Test: shouldGetFiveDayForecastByPostalCode");

        // given
        given(service.getFiveDayForecast(postalCode)).willReturn(result);

        // when
        // then
        final MvcResult mvcResult = mockMvc.perform(get("/forecast/fiveDay/postalCode/{postalCode}", postalCode)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name").value(LOCATION_NAME))
                .andExpect(jsonPath("$[0].dailyForecasts", hasSize(5))).andReturn();
        assertEquals(MediaType.APPLICATION_JSON.toString(), mvcResult.getResponse().getContentType());
    }

    private static Stream<Arguments> shouldGetFiveDayForecastByPostalCode() {
        return Stream.of(Arguments.of("01-248", getFiveDayForecast()));
    }

    private static List<LocationForecast> getFiveDayForecast() {
        return List.of(LocationForecast.builder()
                .name(LOCATION_NAME)
                .dailyForecasts(getDailyForecasts()).build());
    }

    private static List<DailyForecast> getDailyForecasts() {
        return List.of(
                getDailyForecast(OffsetDateTime.now(), 30.5, 20.7),
                getDailyForecast(OffsetDateTime.now().plusDays(1L), 31.5, 21.7),
                getDailyForecast(OffsetDateTime.now().plusDays(2L), 32.5, 22.7),
                getDailyForecast(OffsetDateTime.now().plusDays(3L), 33.5, 23.7),
                getDailyForecast(OffsetDateTime.now().plusDays(4L), 34.5, 24.7));
    }

    private static DailyForecast getDailyForecast(final OffsetDateTime date, final Double maxTemp, final Double minTemp) {
        return DailyForecast.builder()
                .date(date)
                .maximumTemperature(maxTemp)
                .minimumTemperature(minTemp).build();
    }
}
