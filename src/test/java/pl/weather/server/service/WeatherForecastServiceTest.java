package pl.weather.server.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.cloud.client.circuitbreaker.CircuitBreaker;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.web.server.ResponseStatusException;
import pl.weather.client.AccuweatherClient;
import pl.weather.client.config.AccuweatherProperties;
import pl.weather.client.model.DailyForecastResponse;
import pl.weather.client.model.FiveDayForecastResponse;
import pl.weather.client.model.PostalCodeResponse;
import pl.weather.server.model.LocationForecast;

import java.time.OffsetDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
@Slf4j
class WeatherForecastServiceTest {

    @Mock
    private AccuweatherClient accuweatherClient;

    @Mock
    private AccuweatherProperties properties;

    @Mock
    private CircuitBreaker circuitBreaker;

    @Mock
    private CircuitBreakerFactory circuitBreakerFactory; //NOSONAR

    @InjectMocks
    private WeatherForecastService weatherForecastService;

    @DisplayName("Should get five day forecast for a location(s) found by postal code")
    @Test
    void shouldGetFiveDayForecast() {
        log.info("Test: shouldGetFiveDayForecast");

        //given
        final String lang = "pl-pl";
        final boolean metric = true;

        given(accuweatherClient.getFiveDayForecast(anyString(), eq(lang), eq(metric)))
                .willReturn(getFiveDayForecastResponse());
        given(properties.getLocale()).willReturn(lang);
        given(properties.getMetric()).willReturn(metric);
        given(circuitBreakerFactory.create(eq("circuitbreaker"))).willReturn(circuitBreaker); //NOSONAR
        given(circuitBreaker.run(any(), any())).willReturn(getPostalCodeResponses());

        //when
        final List<LocationForecast> locationForecasts = weatherForecastService.getFiveDayForecast("01-248");

        //then
        assertNotNull(locationForecasts);
        assertEquals(2, locationForecasts.size());
    }

    @DisplayName("Should throw ResponseStatusException when empty postal code")
    @Test
    void shouldThrowExceptionWhenEmptyPostalCode() {
        log.info("Test: shouldThrowExceptionWhenEmptyPostalCode");

        Assertions.assertThrows(
                ResponseStatusException.class,
                () -> weatherForecastService.getFiveDayForecast(""));
        Assertions.assertThrows(
                ResponseStatusException.class,
                () -> weatherForecastService.getFiveDayForecast(null));
    }

    private List<PostalCodeResponse> getPostalCodeResponses() {
        return List.of(PostalCodeResponse.builder()
                        .name("Copacabana")
                        .key("123").build(),
                PostalCodeResponse.builder()
                        .name("Kiszki dolne")
                        .key("456").build());
    }

    private FiveDayForecastResponse getFiveDayForecastResponse() {
        return FiveDayForecastResponse.builder()
                .dailyForecasts(List.of(getDailyForecastResponse(), getDailyForecastResponse())).build();
    }

    private DailyForecastResponse getDailyForecastResponse() {
        return DailyForecastResponse.builder()
                .date(OffsetDateTime.now()).build();
    }
}
