package pl.weather.server.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import pl.weather.client.model.DailyForecastResponse;
import pl.weather.client.model.Temperature;
import pl.weather.client.model.TemperatureRange;
import pl.weather.server.model.DailyForecast;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
class DailyForecastMapperTest {

    private static final String temperatureUnit = "C";
    private static final Double maxTemperatureValue = 23.5;
    private static final Double minTemperatureValue = 20.9;
    private static final OffsetDateTime date = OffsetDateTime.now();

    @DisplayName("Should map DailyForecasts from DailyForecastsResponse list")
    @ParameterizedTest
    @MethodSource
    void shouldMapDailyForecasts(
            List<DailyForecastResponse> dailyForecastResponses, List<DailyForecast> expectedResult) {
        log.info("Test: shouldMapDailyForecasts");

        // given
        // when
        final List<DailyForecast> result = DailyForecastMapper.mapDailyForecasts(dailyForecastResponses);
        // then
        assertEquals(result, expectedResult);
    }

    private static Stream<Arguments> shouldMapDailyForecasts() {
        return Stream.of(Arguments.of(getDailyForecastsResponses(), getDailyForecasts()));
    }

    // response

    private static List<DailyForecastResponse> getDailyForecastsResponses() {
        return List.of(getDailyForecastsResponse());
    }

    private static DailyForecastResponse getDailyForecastsResponse() {
        return DailyForecastResponse.builder()
                .date(date)
                .temperatureRange(getTemperatureRange()).build();
    }

    private static TemperatureRange getTemperatureRange() {
        return TemperatureRange.builder()
                .maxTemperature(getTemperature(maxTemperatureValue))
                .minTemperature(getTemperature(minTemperatureValue))
                .build();
    }

    private static Temperature getTemperature(Double value) {
        return Temperature.builder()
                .unit(temperatureUnit)
                .value(value).build();
    }

    // expected result

    private static List<DailyForecast> getDailyForecasts() {
        return List.of(getDailyForecast());
    }

    private static DailyForecast getDailyForecast() {
        return DailyForecast.builder()
                .date(date)
                .maximumTemperature(maxTemperatureValue)
                .minimumTemperature(minTemperatureValue)
                .unit(temperatureUnit).build();
    }
}
