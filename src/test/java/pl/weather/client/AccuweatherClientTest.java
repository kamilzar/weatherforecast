package pl.weather.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import pl.weather.client.config.MockConfiguration;
import pl.weather.client.model.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@SpringBootTest
@ActiveProfiles("test")
@ContextConfiguration(classes = {MockConfiguration.class})
@DirtiesContext
@Slf4j
class AccuweatherClientTest {

    private static final String TOKEN = "TOKEN";

    @Autowired
    private WireMockServer mockServer;

    @Autowired
    private AccuweatherClient client;

    @Autowired
    private ObjectMapper mapper;

    @DisplayName("Should get postal codes")
    @ParameterizedTest
    @MethodSource
    void shouldGetPostalCodes(final String postalCode, final List<PostalCodeResponse> expectedResult) throws Exception {
        log.info("Test: shouldGetPostalCodes");

        // given
        mockServer.stubFor(
                get(urlEqualTo(format("/locations/v1/postalcodes/search?q=%s&apikey=%s", postalCode, TOKEN)))
                        .willReturn(aResponse()
                                .withStatus(HttpStatus.OK.value())
                                .withHeader("Content-Type", APPLICATION_JSON_VALUE)
                                .withBody(mapper.writeValueAsString(expectedResult))));

        // when
        final List<PostalCodeResponse> response = client.getPostalCodes(Map.of("q", postalCode));

        // then
        assertEquals(expectedResult, response);
    }

    private static Stream<Arguments> shouldGetPostalCodes() {
        return Stream.of(Arguments.of("01-248", getPostalCodesBody()));
    }

    private static List<PostalCodeResponse> getPostalCodesBody() {
        return List.of(PostalCodeResponse.builder()
                .name("Svalbard")
                .key("1234").build());
    }

    @DisplayName("Should get five day forecast")
    @ParameterizedTest
    @MethodSource
    void shouldGetFiveDayForecast(
            final String locationKey, final FiveDayForecastResponse expectedResult) throws Exception {
        log.info("Test: shouldGetFiveDayForecast");

        // given
        final String lang = "pl-pl";
        final boolean metric = true;

        mockServer.stubFor(
                get(urlEqualTo(format(
                        "/forecasts/v1/daily/5day/%s?language=%s&metric=%s&apikey=%s", locationKey, lang, metric, TOKEN)))
                        .willReturn(aResponse()
                                .withStatus(HttpStatus.OK.value())
                                .withHeader("Content-Type", APPLICATION_JSON_VALUE)
                                .withBody(mapper.writeValueAsString(expectedResult))));

        // when
        final FiveDayForecastResponse response = client.getFiveDayForecast(locationKey, lang, metric);

        // then
        assertEquals(expectedResult, response);
    }

    private static Stream<Arguments> shouldGetFiveDayForecast() {
        return Stream.of(Arguments.of("1234", getFiveDayForecastResponse()));
    }

    private static FiveDayForecastResponse getFiveDayForecastResponse() {
        return FiveDayForecastResponse.builder().dailyForecasts(List.of(
                getDailyForecastResponse(),
                getDailyForecastResponse(),
                getDailyForecastResponse(),
                getDailyForecastResponse(),
                getDailyForecastResponse())).build();
    }

    private static DailyForecastResponse getDailyForecastResponse() {
        return DailyForecastResponse.builder().temperatureRange(getTemperatureRange()).build();
    }

    private static TemperatureRange getTemperatureRange() {
        return TemperatureRange.builder()
                .maxTemperature(getTemperature())
                .minTemperature(getTemperature()).build();
    }

    private static Temperature getTemperature() {
        return Temperature.builder()
                .unit("C")
                .value(12.3).build();
    }
}
