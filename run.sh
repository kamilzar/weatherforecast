#!/bin/bash

./mvnw clean install -DskipTests
./mvnw spring-boot:build-image
docker-compose up
