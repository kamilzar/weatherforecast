# Five day weather forecast app

* Application gets five day forecast for a postal code.
* Accuweather trial account used by default in configuration allows for about 50 requests per day.
* An account can be switched by providing apiKey token set in property **pl.weather.client.accuweather.token**.

### Environment

* OpenJDK 11.0.11
* Docker 20.10.6
* Apache Maven 3.6.3

## Run instructions

Script:

1. ./run.sh

Docker:

1. ./mvnw clean package
2. ./mvnw spring-boot:build-image
3. docker-compose up

## Testing

* **testing/loadtest.sh** - *performs queries of postal codes in range "00-000" - "00-999"*
* curl http://localhost:8080/forecast/fiveDay/postalCode/{postal_code}
* weatherForecast/http/**fiveDayForecast.http** - *IntelliJ rest client file*

### Swagger

* http://localhost:8080/swagger-ui/#/weather-forecast-controller/getFiveDayForecastByPostalCodeUsingGET

### Prometheus

* HTTP request counter
  - http://localhost:9090/graph?g0.expr=http_server_requests_seconds_count&g0.tab=1&g0.stacked=0&g0.range_input=1h

### Grafana

#### Login: admin

#### Password: admin

* HTTP requests per minute
  - http://localhost:3000/d/hxvcn83Mz/jvm-micrometer?viewPanel=111&orgId=1&refresh=5s&from=now-1m&to=now

![Sample result](testing/RPM_load_test_result.png)
